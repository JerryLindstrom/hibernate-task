package com.example.hibernatetask.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "skills")
public class Skills {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "skill")
    private String skill;

    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = @JoinColumn(name = "skills_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    public List<Teacher> teachers;

    @JsonGetter("teachers")
    public List<String> teachers() {
        return teachers.stream()
                .map(teacher -> {
                    return "/api/v1/teacher/" + teacher.getId();
                }).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

}
