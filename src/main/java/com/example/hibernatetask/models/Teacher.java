package com.example.hibernatetask.models;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "Teacher")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;


    @OneToMany
    @JoinColumn(name = "teacher_id")
    List<Student> students;

    @JsonGetter("students")
    public List<String> students() {
        if(students != null) {
            return students.stream()
                    .map(student -> {
                        return "/api/v1/students/" + student.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "skills_id")
    )
    public List<Skills> skills;

    @JsonGetter("skills")
    public List<String> skills() {
        if (skills != null) {
            return skills.stream()
                    .map(skill -> {
                        return "/api/v1/skill/" + skill.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Skills> getSkills() {
        return skills;
    }

    public void setSkills(List<Skills> skills) {
        this.skills = skills;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
