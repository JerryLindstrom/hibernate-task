package com.example.hibernatetask;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Hibernate API", version = "Beta 0.1", description = "Information about Teachers, Students and Skills"))
public class HibernateTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateTaskApplication.class, args);
    }

}
