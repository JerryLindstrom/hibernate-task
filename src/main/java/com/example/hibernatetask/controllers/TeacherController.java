
package com.example.hibernatetask.controllers;

import com.example.hibernatetask.models.Teacher;
import com.example.hibernatetask.repositories.TeacherRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/teachers")
@Tag(name = "Teachers", description = "This controls the operations for teachers")
public class TeacherController {

    @Autowired
    private TeacherRepository teacherRepository;

    @Operation(summary = "Gets all the teachers")
    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers() {
        List<Teacher> teachers = teacherRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(teachers, status);
    }
    @Operation(summary = "Gets a specific teacher by its unique id")
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacher(@PathVariable Long id) {
        Teacher returnTeacher = new Teacher();
        HttpStatus status;
        if (teacherRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnTeacher = teacherRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnTeacher, status);
    }
    @Operation(summary = "Adds a new teacher if you follow the template")
    @PostMapping
    public ResponseEntity<Teacher> addTeacher(@RequestBody Teacher teacher) {
        HttpStatus status;
        teacher = teacherRepository.save(teacher);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(teacher, status);
    }
    @Operation(summary = "Updates a teacher if you follow the template and the id exists")
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher teacher) {
        Teacher returnTeacher = new Teacher();
        HttpStatus status;
        if (!id.equals(teacher.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnTeacher, status);
        }
        returnTeacher = teacherRepository.save(teacher);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnTeacher, status);
    }
    @Operation(summary = "Changes a teachers first name if the id exists")
    @PatchMapping("/{id}/{firstName}")
    public ResponseEntity<Teacher> modifyTeacher(@PathVariable Long id, @PathVariable String firstName) {
        HttpStatus status;
        if (!teacherRepository.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Teacher teacher = teacherRepository.findById(id).get();
        teacher.setFirstName(firstName);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(teacherRepository.save(teacher), status);
    }
    @Operation(summary = "Deletes a teacher if the id exists", hidden = true)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTeacher(@PathVariable Long id) {
        HttpStatus status;
        if (!teacherRepository.existsById(id)) {
            return new ResponseEntity<>("Teacher with id " + id + " does not exists!", HttpStatus.NOT_FOUND);
        }
        teacherRepository.deleteById(id);
        return new ResponseEntity<>("Teacher with id " + id + " has been deleted!", HttpStatus.OK);
    }
}
