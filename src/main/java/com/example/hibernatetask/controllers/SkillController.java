package com.example.hibernatetask.controllers;

import com.example.hibernatetask.models.Skills;
import com.example.hibernatetask.repositories.SkillsRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/skill")
@Tag(name = "Skills", description = "This controls the operations for skills")
public class SkillController {

        @Autowired
        private SkillsRepository skillsRepository;

        @Operation(summary = "Gets all the skills")
        @GetMapping
        public ResponseEntity<List<Skills>> getAllSkills() {
            List<Skills> skills = skillsRepository.findAll();
            HttpStatus status = HttpStatus.OK;
            return new ResponseEntity<>(skills, status);
        }
        @Operation(summary = "Gets a specific skill by its unique id")
        @GetMapping("/{id}")
        public ResponseEntity<Skills> getSkill(@PathVariable Long id) {
            Skills returnSkills = new Skills();
            HttpStatus status;
            if (skillsRepository.existsById(id)) {
                status = HttpStatus.OK;
                returnSkills = skillsRepository.findById(id).get();
            } else {
                status = HttpStatus.NOT_FOUND;
            }
            return new ResponseEntity<>(returnSkills, status);
        }
        @Operation(summary = "Adds a new skill if you follow the template")
        @PostMapping
        public ResponseEntity<Skills> addSkill(@RequestBody Skills skills) {
            HttpStatus status;
            skills = skillsRepository.save(skills);
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(skills, status);
        }
        @Operation(summary = "Updates a skill if you follow the template and the id exists")
        @PutMapping("/{id}")
        public ResponseEntity<Skills> updateSkills(@PathVariable Long id, @RequestBody Skills skills) {
            Skills returnSkills = new Skills();
            HttpStatus status;
            if (!id.equals(skills.getId())){
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(returnSkills, status);
            }
            returnSkills = skillsRepository.save(skills);
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(returnSkills, status);
        }
        @Operation(summary = "Changes a skill name if the id exists")
        @PatchMapping("/{id}/{skillName}")
        public ResponseEntity<Skills> modifySkills(@PathVariable Long id, @PathVariable String skillName) {
            HttpStatus status;
            if (!skillsRepository.existsById(id)) {
                status = HttpStatus.BAD_REQUEST;
                return new ResponseEntity<>(status);
            }
            Skills skills = skillsRepository.findById(id).get();
            skills.setSkill(skillName);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(skillsRepository.save(skills), status);
        }
        @Operation(summary = "Deletes a skill if the id exists", hidden = true)
        @DeleteMapping("/{id}")
        public ResponseEntity<String> deleteSkills(@PathVariable Long id) {
            HttpStatus status;
            if (!skillsRepository.existsById(id)) {
                return new ResponseEntity<>("Skill with id " + id + " does not exists!", HttpStatus.NOT_FOUND);
            }
            skillsRepository.deleteById(id);
            return new ResponseEntity<>("Skill with id " + id + " has been deleted!", HttpStatus.OK);
        }
    }


