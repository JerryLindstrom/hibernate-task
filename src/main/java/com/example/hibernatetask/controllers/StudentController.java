package com.example.hibernatetask.controllers;


import com.example.hibernatetask.models.Student;
import com.example.hibernatetask.repositories.StudentRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/students")
@Tag(name = "Students", description = "This controls the operations for students")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @Operation(summary = "Gets all the students")
    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents() {
        List<Student> students = studentRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(students, status);
    }
    @Operation(summary = "Gets a specific student by its unique id")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable Long id) {
        Student returnStudent = new Student();
        HttpStatus status;
        if (studentRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnStudent = studentRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnStudent, status);
    }
    @Operation(summary = "Adds a new student if you follow the template")
    @PostMapping
    public ResponseEntity<Student> addStudent(@RequestBody Student student) {
        HttpStatus status;
        student = studentRepository.save(student);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(student, status);
    }
    @Operation(summary = "Updates a student if you follow the template and the id exists")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable Long id, @RequestBody Student student) {
        Student returnStudent = new Student();
        HttpStatus status;
        if (!id.equals(student.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnStudent, status);
        }
        returnStudent = studentRepository.save(student);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnStudent, status);
    }
    @Operation(summary = "Changes a students first name if the id exists")
    @PatchMapping("/{id}/{firstName}")
    public ResponseEntity<Student> modifyStudent(@PathVariable Long id, @PathVariable String firstName) {
        HttpStatus status;
        if (!studentRepository.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
            Student student = studentRepository.findById(id).get();
            student.setFirstName(firstName);
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(studentRepository.save(student), status);
    }

    @Operation(summary = "Deletes a teacher if the id exists", hidden = true)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable Long id) {
        HttpStatus status;
        if (!studentRepository.existsById(id)) {
            return new ResponseEntity<>("Student with id " + id + " does not exists!", HttpStatus.NOT_FOUND);
        }
        studentRepository.deleteById(id);
        return new ResponseEntity<>("Student with id " + id + " has been deleted!", HttpStatus.OK);
    }
}
